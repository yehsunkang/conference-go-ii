# ConferenceGO
A conference & presentation aggregator web app for attendees and presenters.Developed full-stack, single-page web app with Restful API. Combined custom and third party API calls.Implemented poller/ rabbitMQ for communication between microservices.


# Technologies Used
* React
* Django
* Docker
* RESTful API's
* Third-party API's (Pexels, Weather)
* RabbitMQ
* Mailhog


# GHI Design


## Home page
The home page that users will see when they get to the website, listing all the upcoming conference details. Clicking the Attend a conference button leads to the attend conference page to input attendee's info. 

![home page](docs/wireframes/main.png)
![home page continued](docs/wireframes/mainContinued.png)




## Conference Form
A form that can be completed by anyone who would like to create a new conference. The newly created conference dynamically updates on the main page. 

![conference form](docs/wireframes/createConference.png)



## Attend Conference
A form that can be completed where the user chooses the conference they would like to attend and inputs their information. The success message appears when a user successfully signs up for a conference.

![attend conference](docs/wireframes/attend.png)
![success message](docs/wireframes/success.png)