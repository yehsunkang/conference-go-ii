function createCard(name, description, pictureUrl, starts, ends,location ) {
    return `
        <div class="card col-md-3 mx-1" > 
        <div class = "shadow p-3 mb-2 bg-body rounded"> 
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">${starts}-${ends}</div>
          </div>
    </div> `;
  }
  // Module2_2_YehsunKang

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try{
        const response = await fetch(url); 

        if(!response.ok){ // Figure out what to do when the response is bad
            throw new Error('Response not ok');
        } else {
            const data = await response.json();
            console.log(data);
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                const details = await detailResponse.json();
                console.log(details)
                const title = details.name;
                const location = details.location.name
                const description = details.description;
                const pictureUrl = details.location.picture_url;
                const starts= (new Date(details.starts)).toLocaleDateString();
                const ends= (new Date(details.ends)).toLocaleDateString();
                const html = createCard(title, description, pictureUrl, starts, ends, location);
                const row = document.querySelector(".row");
                row.innerHTML += html;

                console.log(html);
            }
              }
            // const conference = data.conferences[1];
            // // console.log(conference);
            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name;

            // const detailUrl = `http://localhost:8000${conference.href}`;
            // const detailResponse = await fetch(detailUrl);
            // if (detailResponse.ok) {
            //     const details = await detailResponse.json();
            //     const description = details.conference.description;
            //     const descriptionTag = document.querySelector('.card-text');
            //     descriptionTag.innerHTML = description;
                
            //     const imageTag = document.querySelector('.card-img-top');
            //     imageTag.src = details.conference.location.picture_url;

            //  console.log(details);

            // }
        }
    } catch (error) { 
        console.error(error);
    //       // Figure out what to do if an error is raised
    // (error) => console.error("error", error);

      }
});

