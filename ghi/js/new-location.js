
//add an event listener for when the DOM loads
window.addEventListener('DOMContentLoaded', async () => {
    //declare a variable that will hold the URL for the API that we just created.
    const url = 'http://localhost:8000/api/states/';
    //fetch the URL with await keyword so that we get the response, not the Promise
    const response = await fetch(url);
    // If the response is okay, then let's get the data using the .json method. 
    // Don't forget to await that, too
    if(response.ok){ //return a promise object
        const data = await response.json();
        console.log(data);
        // Get the select tag element by its id 'state'
        const selectTag = document.getElementById('state');
        // For each state in the states property of the data
        for (let state of data.states){
            // Create an 'option' element
            const option = document.createElement("option")
            // Set the '.value' property of the option element to the
            // state's abbreviation
            option.value = state.abbreviation
            // Set the '.innerHTML' property of the option element to
            // the state's name
            option.innerHTML = state.name
            // Append the option element as a child of the select tag
            selectTag.appendChild(option)
        }
        }
        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
          event.preventDefault();
        //   console.log('need to submit the form data');

        // create a body to use the fetch function to do a POST request
        const formData = new FormData(formTag);
        // the data to send, must be a string
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);
        
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
        method: "post",
        body: json,
        // tell the server that the content you're sending is JSON 
        // with a Content-Type header that has a value of application/json.
        headers: { 
            'Content-Type': 'application/json', // Tell the server it's JSON
        },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
        formTag.reset();
        const newLocation = await response.json();
        console.log(newLocation);
        }
        });
      });


