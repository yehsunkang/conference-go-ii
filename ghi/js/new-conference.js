
//add an event listener for when the DOM loads
window.addEventListener('DOMContentLoaded', async () => {
    //declare a variable that will hold the URL for the API that we just created.
    const url = 'http://localhost:8000/api/locations/';
    //fetch the URL with await keyword so that we get the response, not the Promise
    const response = await fetch(url);
    // If the response is okay, then let's get the data using the .json method. 
    // Don't forget to await that, too
    if(response.ok){ //return a promise object
        const data = await response.json();
        console.log(data);
        // Get the select tag element by its id 'state'
        const selectTag = document.getElementById('location');
        console.log(selectTag);
        // For each state in the states property of the data
        for (let location of data.locations){
            // Create an 'option' element
            const option = document.createElement("option")
            // Set the '.value' property of the option element to the
            // state's abbreviation
            option.value = location.id
            // Set the '.innerHTML' property of the option element to
            // the state's name
            option.innerHTML = location.name
            // Append the option element as a child of the select tag
            selectTag.appendChild(option)
        }
        }
        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
          event.preventDefault();
        //   console.log('need to submit the form data');
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);
        
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
        formTag.reset();
        const newConference = await response.json();
        console.log(newConference);
        }
        });
      });


