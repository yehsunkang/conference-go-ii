window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
  
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      const loadingTag = document.getElementById("loading-conference-spinner");
      loadingTag.classList.add("d-none");
      selectTag.classList.remove("d-none");
    }
    const formTag = document.getElementById("create-attendee-form");
        formTag.addEventListener('submit', async event => {
          event.preventDefault();
        //   console.log('need to submit the form data');
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);
        const attendeesUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(attendeesUrl, fetchConfig);
        if (response.ok) {
        formTag.reset();
        const attendeeConference = await response.json();
        console.log(attendeeConference);

        const successAlert = document.getElementById("success-message");
        formTag.classList.add("d-none");
        successAlert.classList.remove("d-none");
        }
        });
  });
  