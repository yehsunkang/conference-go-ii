
import Nav from './Nav';
import React from 'react';
import MainPage from './MainPage';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import Presentationform from './Presentationform';
import AttendConferenceForm from './AttendConferenceForm';
import { BrowserRouter, Routes, Route } from "react-router-dom";


function App(props) {
console.log(props.attendees)
  if (props.attendees === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      {/* <div className="container"> */} 
        <Routes>
        <Route index element={<MainPage />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<Presentationform />} />
          </Route>
          <Route path="attendees">
            <Route path="" element={<AttendeesList attendees={props.attendees} />} />
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
        </Routes>
      {/* </div> */}
    </BrowserRouter>
  );
}

export default App;
